package com.employeedetails.employeedetails.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.employeedetails.employeedetails.bean.Employee;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Integer> {

}
