package com.employeedetails.employeedetails.service;

import org.springframework.stereotype.Service;

import com.employeedetails.employeedetails.bean.Employee;

@Service
public interface EmployeeService {
	public Employee create(Employee employee);

	public Iterable<Employee> findAll();

}
