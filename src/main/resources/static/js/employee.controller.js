app.controller("employeeController", ['$scope', 'httpService',
    employeeController]);

function employeeController($scope, httpService) {
    $scope.headingTitle = "Employee List";
    $scope.employeeList = [];
    $scope.countryList = [];
    $scope.stateList = [];
    $scope.actionLabel = "Create";
    $scope.employee = {};
    $scope.init = function () {
        httpService.findAll('employee/findAll').then(function onSuccess(response) {
            $scope.employeeList = response;
        }, function onError(error) {
            console.log("Unknown error");
        });
    }
    $scope.getAll = function () {
        httpService.getJson('assets/country-state.json').then(function onSuccess(response) {
            $scope.countryList = response.countries;
        }, function onError(error) {
            console.log("Unknown error");
        });
    }
    $scope.init();
    $scope.getAll();
    $scope.create = function () {
        httpService.create('employee/create', $scope.employee).then(function onSuccess(response) {
            $scope.init();
            $scope.employee = {};
        }, function onError(error) {
            console.log("Unknown error");
        });
    }
    $scope.setStates = function (country) {
        var temp = $scope.countryList.filter((c) => c.country === country)[0];
        $scope.stateList = temp.states;
    }
}