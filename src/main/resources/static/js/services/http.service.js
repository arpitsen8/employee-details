var baseUrl = "http://localhost:8081/";
app.factory("httpService", function($http) {
    return {
        findAll: function(URL) {
            return $http.get(baseUrl+URL).then(function(response) {
                return response.data;
            });
        },
        create: function(URL,data) {
            return $http.post(baseUrl+URL,data).then(function(response) {
                return response.data;
            });
        },
        getJson: function(URL) {
            return $http.get(URL).then(function(response) {
                return response.data;
            });
        }
    }
})